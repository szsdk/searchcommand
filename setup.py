from setuptools import setup

setup(
    name             = 'searchcommand',
    version          = '0.1',
    author           = 'szsdk',
    packages = ['searchcommand'],
    # py_modules       = ['searchcommand', 'searchcommand._sc_main', 'searchcommand.searchsession',  'searchcommand.sc_client'],
    # py_modules       = ['searchcommand'],
    scripts=['scripts/sc_s', 'scripts/sc_c', 'scripts/sc'],
    install_requires = [
        'termcolor', 'pyzmq'
    ],
    # include_package_data=True,
)
