import itertools as it
import re
import sys
from termcolor import colored

__all__ = [
        'Command',
        'TextFile',
        'CommandRegister',
        'BaseCommand',
        ]


def inject_command(cmd):
    import fcntl, termios
    for c in cmd:
        fcntl.ioctl(sys.stdin, termios.TIOCSTI, c)

class CommandRegister(type):
    register = set()

    def __new__(cls, name, bases, dct):
        x = super().__new__(cls, name, bases, dct)
        x._snake_name = CommandRegister.convert(name)
        cls.register.add(x)
        return x

    @staticmethod
    def convert(name):
        s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


class BaseCommand(metaclass=CommandRegister):
    def __init__(self, key_words, command, description, *args, **kargs):
        if not isinstance(key_words, list):
            key_words = [key_words]
        self.key_words   = key_words
        self.command     = command
        self.description = description

    def __contains__(self, input_words):
        if not isinstance(input_words, list):
            input_words = [input_words]
        for k in input_words:
            findQ = False
            for kw in self.key_words:
                if k in kw:
                    findQ = True
            if findQ or (k in self.command):
                continue
            return False
        return True

    def str_command(self):
        return self.command

    def preview(self, query):
        pv = self.command
        if self.description != "":
            pv += colored("\nDescription:", "green") + f"\n{self.description}"
        if len(self.key_words) > 0:
            pv += colored("\nKeywords:", "green") + f"\n{', '.join(self.key_words)}"
        return pv

    def run(self, command):
        raise NotImplementedError

    def result(self):
        raise NotImplementedError

class Command(BaseCommand):
    def run(self, command):
        inject_command(self.command)
        # print(self.command)

    def result(self):
        return self.command

class TextFile(BaseCommand):
    editor = 'vim %s'
    def __init__(self, key_words, filename, description, *args, **kargs):
        super(TextFile, self).__init__(key_words, str(filename), description,
                )

    def run(self, command):
        import os
        os.system(self.editor % self.command)
