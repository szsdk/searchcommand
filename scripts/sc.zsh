function _sc {
    #zle kill-whole-line
    #zle zle kill-buffer && zle -R
    #sc
    #zle -U "git status"
    #zle accept-line
    #
    local PORT result
    PORT=`mktemp`
    (sc_s server $PORT &)
    result=`echo '' | fzf --preview-window=down --preview 'sc_c '$PORT' preview {} {q}' --bind 'ctrl-y:execute(sc_c '$PORT' result {} | xclip -selection c | echo)+abort' --bind 'change:reload(sc_c '$PORT' list {q})' --phony`
    [[ ! -z "$result" ]] && sc_s run "$result"
    sc_c $PORT command exit
    rm $PORT
    #res="ls -all"
    #zle kill-buffer && zle -R
    zle reset-prompt
    BUFFER=23
    #[[ ! -z "$res" ]] && BUFFER=${res}
    #res=""
    #CURSOR=${res:0:2}
}

zle -N _sc
bindkey '\eg' _sc
